variable "region" {
  description = "AWS region"
}

variable "ami" {
  type        = "map"
  description = "AMI to use"
  default     = {}
}

variable "instance_type" {
  description = "AWS instance type"
  default     = "t2.micro"
}

provider "aws" {
  region = "${var.region}"
}

terraform {
  backend "s3" {
    bucket = "mirc-inc-remote-state-development"
    key    = "terraform.tfstate"
    region = "us-east-1"
  }
}

module "vpc" {
  source        = "git::ssh://mircea-colo@bitbucket.org/mircea-colo/tf_aws_vpc.git"
  name          = "web"
  cidr_block    = "10.0.0.0/16"
  subnet_public = "10.0.1.0/24"
}

resource "aws_instance" "web" {
  ami                         = "${lookup(var.ami, var.region)}"
  instance_type               = "${var.instance_type}"
  subnet_id                   = "${module.vpc.public_subnet_id}"
  associate_public_ip_address = true

  vpc_security_group_ids = [
    "${aws_security_group.sec_group.id}",
  ]

  count = 2
}

resource "aws_elb" "web_lb" {
  name            = "web-elb"
  subnets         = ["${module.vpc.public_subnet_id}"]
  security_groups = ["${module.security-group.this_security_group_id}"]

  listener {
    instance_port     = 8000
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  instances = ["${aws_instance.web.*.id}"]
}

module "security-group" {
  source              = "terraform-aws-modules/security-group/aws"
  name                = "elb-sec-group"
  description         = "ELB security group with HTTP ports open"
  vpc_id              = "${module.vpc.vpc_id}"
  ingress_rules       = ["http-80-tcp"]
  ingress_cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_security_group" "sec_group" {
  name        = "web-sec-group"
  description = "SSH and HTTP to web hosts"
  vpc_id      = "${module.vpc.vpc_id}"

  ingress {
    from_port   = 8000
    to_port     = 8000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
